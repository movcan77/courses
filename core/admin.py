from django.contrib import admin

# Register your models here.
from django.contrib.auth.admin import UserAdmin
from pkg_resources import _

from core.models import models, Student

for model in models:
    admin.site.register(model)


class UserProfileAdmin(UserAdmin):
    fieldsets = (
        (None, {
            'fields': ('username', 'password')}),
        (_('Personal info'), {
           'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
        (_('Important dates'), {
            'fields': ('last_login', 'date_joined')}),
    )


admin.site.register(Student, UserProfileAdmin)
