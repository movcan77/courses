from django.contrib.auth.models import AbstractUser
from django.db import models


# Create your models here.
class Student(AbstractUser):
    course = models.ManyToManyField('Course', related_name='course')
    lecture_finished = models.ManyToManyField('Lecture')
    course_finished = models.ManyToManyField('Course', related_name='course_finished')


class Course(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    price = models.IntegerField()
    is_enabled = models.BooleanField()
    category = models.ForeignKey('Category', on_delete=models.SET_NULL, blank=True, null=True)
    photo = models.ForeignKey('Photo', on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return self.title


class Lecture(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    video = models.CharField(max_length=255)
    course = models.ForeignKey('Course', on_delete=models.CASCADE)
    photo = models.ForeignKey('Photo', on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return self.title


class Comment(models.Model):
    user = models.ForeignKey('Student', on_delete=models.CASCADE)
    course = models.ForeignKey('Course', on_delete=models.CASCADE)
    message = models.TextField()
    is_approved = models.BooleanField()

    def __str__(self):
        return self.user


class Homework(models.Model):
    user = models.ForeignKey('Student', on_delete=models.CASCADE)
    lecture = models.ForeignKey('Lecture', on_delete=models.CASCADE)
    description = models.TextField()

    def __str__(self):
        return self.description


class Category(models.Model):
    title = models.CharField(max_length=255)
    is_enabled = models.BooleanField()

    def __str__(self):
        return self.title


class Photo(models.Model):
    title = models.CharField(max_length=255)
    # thumbnail = models.FileField
    image = models.FileField()

    def __str__(self):
        return self.title


class Subscription(models.Model):
    user = models.ForeignKey('Student', on_delete=models.CASCADE)
    course = models.ForeignKey('Course', on_delete=models.CASCADE)
    payment_id = models.CharField(max_length=255)
    payment_message = models.CharField(max_length=255)
    payment_status = models.CharField(max_length=255)
    date = models.DateTimeField()

    def __str__(self):
        return self.date.ctime()


models = [Course, Lecture, Comment, Subscription, Photo, Category, Homework]